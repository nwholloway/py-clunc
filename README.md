# CLUNC

This is a Python implementation of `clunc`
([Client for LaCie U-Boot NetConsole](http://lacie-nas.org/doku.php?id=clunc)) inspired by the
implementation available at [http://git.lacie-nas.org/clunc.git](http://git.lacie-nas.org/?p=clunc.git;a=summary).

I decided to write this after spending a few hours trying to work out what combination of `clunc` and `nc`
were required for this to work.

This is implemented as a single Python script, rather than the combination of C executable, shell script and
calls to netcat.  This means the only dependency is Python 3.  I have tried to steer away from optional
modules and new language features.

## Usage

```
usage: clunc [-h] [-v] [-m MAC] -i IP [-w seconds]

Client for LaCie U-Boot NetConsole

options:
  -h          show this help message and exit
  -v          verbose
  -m MAC      MAC to target
  -i IP       IP to set (mandatory)
  -w seconds  inactivity timeout between batch session commands (default: 1)
```

Input can either be typed interactively, pasted into console, or piped in
from a file.  Multi-line pastes are treated like non-interactive input from
a file, and will wait for the inactivity timeout before sending each line.

## Operation

You specify the IP address for the device to use with the `-i` option
(select an unused address suitable for your network).  You may be able to
prevent responses from devices other than the one you want by specifying
the MAC (`-m`).

If the device is already in "NetConsole" mode (responds to `^C`), then
the next activation step is not required.

To activate, a LUMP packet is broadcast to port 4446 every 0.5 second.
This contains the IP address for the device to use.  Receiving this packet
will cause the device to stop the normal boot process, and switch to
"NetConsole" mode.  This is detected by receiving a response (a prompt)
from the device.

The console connection allows you to sent U-Boot commands to the device.
The commands and responses are exchanged over a UDP connection on
port 6666.

To avoid dropped packets, all output from the device is read before
sending input.  When used interactively, input is sent a character at
a time.  When input is from a file, input is sent a line at a time, and
the next line is not sent until the inactivity timeout (`-w`) has expired.

Use `^C` to terminate.